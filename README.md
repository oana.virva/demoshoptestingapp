# Final project for Demo shop application testing

## Short Description
This application is responsible for testing the main functionalities of the Demo Shop app.

- Product Listing
- Login
- Add to Cart
- Add To Wishlist
- StaticApplicationTest

# Demo Shop Test Automation Framework
A brief presentation of my project

## This is the final project for Ioana Virva, within the FastTrackIt Test Automation course.


### Tech stack used:
    - Java17
    - Maven
    - Selenide Framwork
    - PageObject Models

### How to run the tests
`git clone https://gitlab.com/oana.virva/demoshoptestingapp.git
Execute the following commands to:
#### Execute all tests
- `mvn clean tests`
#### Generate Report
- `mvn allure:report`
> Open and present report
- `mvn allure:serve`

#### Page Objects
    - HomePage
    - Header
    - Footer
    - Body / Products

### Tests implemented
| Test Name                      | Execution Status  | Date       |
|--------------------------------|-------------------|------------|
| Click on product for details   | **PASSED**        | 10.04.2023 |
| Login Test with different user | **PASSED**        | 10.04.2023 |
| Verify Modal components        | **PASSED**        | 10.04.2023 |
| Verify Footer components       | **PASSED**        | 10.04.2023 |
| Verify Header components       | **PASSED**        | 10.04.2023 |
| Add product to Wishlist        | **PASSED**        | 10.04.2023 |
| Add product to Cart            | **PASSED**        | 10.04.2023 |


- Reporting is also available in the Allure-results folder
  - tests are executed using maven


### Tests defintion
| TestID | Test Name | Test Description | Test Status|

