package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Header;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class StaticApplicationStateTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @Test(testName = "Verify Demo shop Title")
    public void verify_Demo_Shop_App_Title() {
        String appTitle = homePage.getPageTitle();
        assertEquals(appTitle, "Demo shop", "Application title is expected to be Demo shop");
    }

    @Test(testName = "Verify Demo Shop Footer Details")
    public void verify_Demo_Shop_Footer_Build_Date_Details() {
        String footerDetails = homePage.getFooter().getDetails();
        assertEquals(footerDetails, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT ");
    }

    @Test(testName = "Verify Footer contains Question Icon")
    public void verify_Demo_Shop_Footer_Contains_Question_Icon() {
        SelenideElement questionIcon = homePage.getFooter().getQuestionIcon();
        assertTrue(questionIcon.exists(), "Expected Question Icon to exist on page");
        assertTrue(questionIcon.isDisplayed(), "Expected Question Icon to be displayed");

    }

    @Test(testName = "Verify Footer contains Reset Icon")
    public void verify_Demo_Shop_Footer_Contains_Reset_Icon() {
        SelenideElement resetIconTitle = homePage.getFooter().getResetIconTitle();
        assertTrue(resetIconTitle.exists(), "Expected Reset Icon to exist on page");
        assertTrue(resetIconTitle.isDisplayed(), "Expected Reset Icon to be displayed");

    }

    @Test(testName = "Verify Header contains Logo Icon")
    public void verify_Header_Contains_Logo_Icon_Url() {
        SelenideElement logoIconUrl = homePage.getHeader().getLogoIconUrl();
        assertTrue(logoIconUrl.exists(), "Expected Logo Icon Url to exist on page");
        assertTrue(logoIconUrl.isDisplayed(), "Expected Reset Icon to be displayed");
    }

    @Test(testName = "Verify Header contains Shopping Cart Icon")
    public void verify_Header_Contains_Shopping_Cart_Icon_Url() {
        assertTrue(homePage.validateShoppingCartBadgeIsDisplayed(), "Expected Shopping Cart Icon to be displayed");
    }
    @Test(testName = "Verify Header contains Wishlist Icon")
    public void verify_Header_Contains_Wishlist_Icon_Url() {
        assertTrue(homePage.validateThatTheWishlistIsDisplayed(), "Expected Shopping Cart Icon to be displayed");
    }

    @Test(testName = "Verify Header contains Welcome Message")
    public void verify_Header_Contains_Welcome_Message() {
        String welcomeMessage = homePage.getHeader().getGreetingsMessage();
        assertEquals(welcomeMessage, "Hello Guest", "welcome Message is expected to be Hello guest!");
    }

    @Test(testName = "Verify Header contains Login Button")
    public void verify_Header_Contains_Login_Button() {
        SelenideElement loginButton = homePage.getHeader().getLoginButton();
        assertTrue(loginButton.exists(), "Expected Sign In Button to exist on page");
        assertTrue(loginButton.isDisplayed(), "Expected Sign In Button to be displayed");
    }

}


