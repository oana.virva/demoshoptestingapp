package org.fasttrackit;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DataProvider.ProductDataProvider;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import products.Product;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProductTest extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
    }

    @Test(testName = "Click on product for details",
            dataProvider = "ProductsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_click_on_a_product(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = p.getExpectedResults().getTitle();
        Assert.assertEquals(productPage.getTitle(), expectedTitle, "Expected Title to be" + expectedTitle);
    }


    @Test(testName = "Add product to Wishlist",
            dataProvider = "ProductsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_wishlist(Product p) {
        p.AddProductToFavorites();
        sleep(3000);
        assertTrue(p.validateProductIsAddedToWishlist(), "Product was added to Favorites");

    }

    @Test(testName = "Add product to Cart",
            dataProvider = "ProductsDataProvider", dataProviderClass = ProductDataProvider.class)
    public void user_can_add_product_to_cart(Product p) {
        p.addProductToBasket();
        sleep(3000);
        assertTrue(p.validateProductIsAddedToCart(), "Product was added to Cart");

    }



}
