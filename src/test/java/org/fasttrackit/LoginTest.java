package org.fasttrackit;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DataProvider.AccountDataProvider;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import products.Account;

import static java.lang.Thread.sleep;
import static org.testng.Assert.*;

public class LoginTest extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToTheHomePage();

    }

    @AfterMethod
    public void tearDown() {
        System.out.println("I will clean up after each test!");
    }


    @Test(testName = "Login test without password",
            description = "This test is responsible for testing that the user can not login on page without password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_Cannot_Login_On_Page_Without_Password() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String user = "beetle";
        modal.typeInUsernameField(user);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected Modal to remain on page");
        String msgError = modal.getErrorMessage();
        assertEquals(msgError, "Please fill in the password!", "Expected message error to appear");
        modal.clickOnCloseButton();


    }

    @Test(testName = "Login test without userName",
            description = "This test is responsible for testing that the user can not login on page without userName")
    @Severity(SeverityLevel.CRITICAL)
    public void user_Cannot_Login_On_Page_Without_UserName() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        String password = "choochoo";
        modal.typeInPasswordField(password);
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected Modal to remain on page");
        String msgError = modal.getErrorMessage();
        assertEquals(msgError, "Please fill in the username!", "Expected message error to appear");
        modal.clickOnCloseButton();
    }


    @Test(testName = "Verify Modal components and modal can be closed")
    public void modal_Components_are_Displayed_and_Modal_Can_Be_closed() {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal is displayed");
        assertEquals(modal.getModalTitle(), "Login", "Expect Modal Title to be Login.");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close Button to be displayed.");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected Username Field to be displayed.");
        assertTrue(modal.validatePasswordFieldIsDisplayed(),"Expected Password Field to be displayed.");
        assertTrue(modal.validateLoginButtonIsDisplayedAndEnabled(),"Expected Password Field to be displayed.");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected Modal to be closed!");


    }


    @Test(testName = "Login test with valid user",
            dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class)
    public void user_Can_Login_On_The_Demo_App(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUserName());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected Modal to be closed!");
        Header header = new Header(account.getUserName());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMessage(), "Expected greetings message to contain the account user");
        homePage.logUserOut();
    }

    @Test(testName = "Login test with invalid user")
    public void user_cannot_Login_With_Locked_Account() {
        Account lockedAccount = new Account("locked", "choochoo");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUserName());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected Modal to remain open");
        String msgError = modal.getErrorMessage();
        assertEquals(msgError, "The user has been locked out.", "Expected message error to appear");
        modal.clickOnCloseButton();

    }







}
