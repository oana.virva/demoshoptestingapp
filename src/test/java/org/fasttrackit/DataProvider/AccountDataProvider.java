package org.fasttrackit.DataProvider;

import products.Account;
import org.testng.annotations.DataProvider;

public class AccountDataProvider {
    @DataProvider(name = "AccountDataProvider")
    public static Object[][] createAccountsProvider() {
        Account beetleaccount = new Account("beetle", "choochoo");
        Account dinoaccount = new Account("dino", "choochoo");
        Account turtleaccount = new Account("turtle", "choochoo");
        return new Object[][]{
                {beetleaccount},
                {dinoaccount},
                {turtleaccount}
        };

    }
}