package products;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement productLink ;
    private final SelenideElement card;
    private final SelenideElement addToBasketButton;
    private final SelenideElement addToFavorites;
    private final ProductExpectedResults expectedResults;

    private final SelenideElement productAddedToCart;

    private final SelenideElement productIsAddedToFavorites = $(".shopping-cart-icon .fa-heart~span");
    private final SelenideElement productIsAddedToCart = $(".shopping-cart-icon .fa-shopping-cart~span");
    private final SelenideElement totalPrice = $(".amount-total");
    public Product(String productId, ProductExpectedResults expectedResults) {
        this.productLink = $(String.format(".card-body [href='#/product/%s']", productId));
        this.expectedResults = expectedResults;
        this.card = productLink.parent().parent();
        this.addToBasketButton = card.$(".fa-cart-plus");
        this.addToFavorites = card.$(".fa-heart");
        this.productAddedToCart = card.$(".shopping-cart-icon .fa-shopping-cart~span");


    }

    public void clickOnProduct () {
        this.productLink.click();
    }

    public void addProductToBasket () {
        this.addToBasketButton.click();
    }

    public void AddProductToFavorites (){
        this.addToFavorites.click();
    }

    @Override
    public String toString() {
        return this.productLink.getText();
    }

    public ProductExpectedResults getExpectedResults() {
        return expectedResults;
    }

    public boolean validateProductIsAddedToCart (){
        return this.productIsAddedToCart.exists() && this.productIsAddedToCart.isDisplayed();
    }

    public String getProductIsAddedToFavorites() {
        return productIsAddedToFavorites.getText();
    }

    public String getTotalPrice() {
        return totalPrice.getText();
    }

    public boolean validateProductIsAddedToWishlist () {
        return this.productIsAddedToFavorites.exists() ;

    }






}

