package products;

public class Account {
    private final String userName;
    private final String password;
    private final String greetingsMessage;

    public Account(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.greetingsMessage = "Hi " + this.userName + "!";
    }


    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getGreetingsMessage() {
        return greetingsMessage;
    }

    @Override
    public String toString() {
        return this.userName;
    }
}
