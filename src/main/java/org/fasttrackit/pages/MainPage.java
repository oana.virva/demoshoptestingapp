package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class MainPage extends Page {
    private final String title = Selenide.title();


    private Header header;
    private final Footer footer;
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement cartBadge = $(".fa-shopping-cart");

    private final SelenideElement wishlistIcon = $("[href='#/wishlist']");


    public MainPage() {
        this.header = new Header();
        this.footer = new Footer();
    }

    public SelenideElement getWishlistIcon() {
        return wishlistIcon;
    }

    public boolean validateThatTheWishlistIsDisplayed() {
        System.out.println(" Verify that Wishlist page is displayed");
        return this.wishlistIcon.exists() && this.wishlistIcon.isDisplayed();
    }

    public void returnToTheHomePage() {
        this.header.getLogoIconUrl().click();
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return this.header;
    }

    public String getPageTitle() {
        return title;
    }


    public Footer getFooter() {
        return footer;
    }

    public void validateThatFooterContainsAllElements() {
    }

    public void validateThatHeaderContainsAllElements() {
    }

    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that Modal is displayed on page ");
        return this.modal.exists() && this.modal.isDisplayed();

    }

    public boolean validateModalIsNotDisplayed() {
        System.out.println("Verify that Modal is not on page ");
        return !modal.exists();

    }

    public boolean validateShoppingCartBadgeIsDisplayed() {
        System.out.println("Verify that Shopping Cart Badge is on page ");
        return cartBadge.exists() && this.cartBadge.isDisplayed();
    }


    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
        sleep(150);
    }


    public void clickOnTheWishListButton() {
        this.header.clickOnTheWishlistIcon();
    }

    public void clickOnTheLogoButton() {
        this.header.clickOnTheLogoIconUrl();
    }


    public void logUserOut() {
        this.header.clickOnTheLogOutButton();
        sleep(150);
    }


}
