package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WishlistPage extends MainPage {
    private final SelenideElement pageSubtitle = $(".text-muted");

    private final SelenideElement wishlistIcon = $("[href='#/wishlist']");


    public SelenideElement getWishlistIcon() {
        return wishlistIcon;
    }


    public WishlistPage() {
        System.out.println("Wish list page was created!");
    }


    public boolean validateThatTheWishlistIsDisplayed() {
        System.out.println(" Verify that Wishlist page is displayed");
        return this.wishlistIcon.exists() && this.wishlistIcon.isDisplayed();
    }



}