package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailsPage {
    private final SelenideElement title = $(".text-muted");


    public String getTitle() {
        return title.text();
    }
}
