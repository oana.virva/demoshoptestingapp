package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement logoIconUrl = $(".fa-shopping-bag");
    private final String shoppingCartIconUrl;
    private final SelenideElement wishListIconUrl = $(".shopping-cart-icon .fa-heart~span");
    private final String greetingsMessage;
    private final SelenideElement signInButton = $(".fa-sign-in-alt");

    private final SelenideElement signOutButton = $(".fa-sign-out-alt");

    public Header() {
        this.shoppingCartIconUrl = "/cart";
        this.greetingsMessage = "Hello Guest";
    }

    public Header(String user) {
        this.shoppingCartIconUrl = "/cart";
        this.greetingsMessage = "Hi " + user + "!";

    }

    /**
     * Getters
     */

    public SelenideElement getLogoIconUrl() {
        return this.logoIconUrl;
    }

    public String getShoppingCartIconUrl() {
        return this.shoppingCartIconUrl;
    }

    public SelenideElement getWishListIconUrl() {
        return this.wishListIconUrl;
    }

    public String getGreetingsMessage() {
        return this.greetingsMessage;
    }

    public SelenideElement getLoginButton() {
        return this.signInButton;
    }

    /**
     * Actions
     */

    public void clickOnTheLoginButton() {
        System.out.println("Clicked on the Login button");
        this.signInButton.click();
    }

    public void clickOnTheLogOutButton() {
        this.signOutButton.click();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Clicked on the Wishlist button");
    }

    public void clickOnTheLogoIconUrl() {
        this.logoIconUrl.click();
        System.out.println("Clicked on the Logo Icon Url");

    }

}
