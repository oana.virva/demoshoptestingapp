package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement userName = $("#user-name");

    private final SelenideElement password = $("#password");

    private final SelenideElement loginButton = $(".btn-primary");

    private final SelenideElement errorMessage = $(".error");


    public String getModalTitle() {
        return this.modalTitle.text();
    }

    public String getErrorMessage() {
        return this.errorMessage.getText();
    }
    public void validateModalComponents() {
    }


    /**
     * Clicks
     */

    public void clickOnCloseButton() {
        System.out.println(" Clicked on the X button ");
        this.closeButton.click();
        sleep(150);


    }

    public void clickOnUsernameField() {
        this.userName.click();
    }

    public void clickOnPasswordField() {
        System.out.println(" Clicked on the: " + this.password);
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        System.out.println(" Clicked on the Login Button");
        sleep(250);
    }

    /**
     * Type Actions
     */

    public void typeInPasswordField(String passwordToType) {
        System.out.println(" Typed in password:" + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    public void typeInUsernameField(String userToType) {
        System.out.println(" Typed in username:" + userToType);
        this.userName.click();
        this.userName.sendKeys(userToType);
    }

    /**
     * Validators
     */
    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.userName.exists() && this.userName.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {
        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonIsDisplayedAndEnabled() {
        return this.loginButton.exists() && this.loginButton.isEnabled();
    }


}
